module testplugins.one{
	exports de.grogra.testplugins.one;

	requires java.desktop;

	requires imp;
	requires platform;
	requires platform.core;
	requires utilities;
	requires graph;
	requires xl;
	requires xl.core;
}
